CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Drush Config Import Log module allows the user to track the configuration
import log in the database and also in text file. This will be useful on a live site
where there are no permissions to run config import by CLI.

 * For a full description of the module visit:
   https://www.drupal.org/project/drush_config_import_log

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/drush_config_import_log


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Drush Config Import Log module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. From the module page, select the Configure contextual link next to the
       module to configure the settings (admin/config/RunCustomImport). From
       here, there is the choice to check the Log File or Run Import.
    3. Importing the log will track in recent log and also in the text file.


MAINTAINERS
-----------

 * saranya purushothaman - https://www.drupal.org/u/saranya-purushothaman

Supporting organization:

 * UniMity Solutions Pvt Limited -
   https://www.drupal.org/unimity-solutions-pvt-limited
